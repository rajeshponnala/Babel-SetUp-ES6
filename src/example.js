var listeners = []
function listen () {}
var events = {listeners, listen}
var timer = {
  seconds: 0,
  start () {
    setInterval(() => {
      this.seconds ++
    }, 1000)
  }
}
timer.start()
setTimeout(function () {
  console.log(timer.seconds)
}, 3500)

console.log([1, 2, 3, 4]
  .map(value => value * 2)
  .filter(value => value > 2)
  .forEach(value => console.log(value)))
var character = {
  name: 'Bruce',
  pseudonym: 'Batman',
  metadata: {
    age: 34,
    gender: 'male'
  },
  batarang: ['gas pellet', 'bat-mobile control', 'bat-cuffs']
}
var { pseudonym, name } = character
// console.log(pseudonym + ' ' + name)
const coordinates = [12, -7]
var [x] = coordinates
console.log(x)
